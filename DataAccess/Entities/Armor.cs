﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class Armor
    {
        public int Id { get; set; }
        public string ArmorName { get; set; }
        public int ArmorBuff { get; set; }
    }
}
