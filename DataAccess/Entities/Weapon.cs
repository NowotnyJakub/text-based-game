﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class Weapon
    {
        public int Id { get; set; }
        public string WeaponName { get; set; }
        public int WeaponBuff { get; set; }
    }
}
